from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from api.views import ScraperAPI
from webscraper.views import HomeView
from api.services.thread_runner_service import ThreadRunnerService

threads = ThreadRunnerService()
threads.run_inital_scrapers()

urlpatterns = [
      path('', HomeView.as_view(), name='home'),
      path('api/scrapers/', ScraperAPI.as_view(), name='scrapers'),
      path('admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
