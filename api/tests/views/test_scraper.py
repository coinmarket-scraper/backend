from django.test import TestCase, Client
from api.models import Scraper
from django.urls import reverse
from api.utils.prevent_warnings import prevent_request_warnings

class ScraperViewTest(TestCase):

    client = Client()

    def test_get_request(self):
        response = self.client.get(
            reverse('scrapers'),
            content_type='application/json'
        )
        self.assertIsNotNone(response)

    @prevent_request_warnings
    def test_delete_request(self):
        response = self.client.delete(
            reverse('scrapers'),
            data="{}",
            content_type='application/json'
        )
        self.assertIsNotNone(response)
    
    @prevent_request_warnings
    def test_patch_request(self):
        response = self.client.put(
            reverse('scrapers'),
            data="{}",
            content_type='application/json'
        )
        self.assertIsNotNone(response)
    
    @prevent_request_warnings
    def test_post_request(self):
        response = self.client.post(
            reverse('scrapers'),
            data="{}",
            content_type='application/json'
        )
        self.assertIsNotNone(response)
