from django.test import TestCase
from api.scrapers.currency_scraper import CurrencyScraper


class ScraperTest(TestCase):
    
    def setUp(self):
        self.valid_currency = 'bitcoin'
        self.invalid_currency = 'asdasd'

    def test_valid_currency_scraper(self):
        currency_scraper = CurrencyScraper(self.valid_currency)
        price = currency_scraper.run_test()
        self.assertTrue(price > 0)   

    def test_invalid_currency_scraper(self):
        currency_scraper = CurrencyScraper(self.invalid_currency)
        price = currency_scraper.run_test()
        self.assertTrue(price == 0)      