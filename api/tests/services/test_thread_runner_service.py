from django.test import TestCase
from api.services.thread_runner_service import ThreadRunnerService, threads_running, Scraper

class ThreadRunnerTest(TestCase):

    thread_runner_service = ThreadRunnerService()

    @classmethod
    def setUpTestData(cls):
        Scraper.objects.create(currency='bitcoin', frequency=5)
    
    def test_start_initial_scrapers(self):
        scraper = Scraper.objects.get(pk=1)
        self.thread_runner_service.run_inital_scrapers()
        self.assertEqual(len(threads_running), 1)
        self.thread_runner_service.stop_thread(scraper.currency)
        self.assertEqual(len(threads_running), 0)
    
    def test_start_new_thread(self):
        scraper = Scraper.objects.get(pk=1)
        self.thread_runner_service.run_new_currency_thread(scraper)
        self.assertEqual(len(threads_running), 1)
        self.thread_runner_service.stop_thread(scraper.currency)
        self.assertEqual(len(threads_running), 0)
    
    def test_update_frequency_thread(self):
        scraper = Scraper.objects.get(pk=1)
        self.thread_runner_service.run_new_currency_thread(scraper)
        self.assertEqual(len(threads_running), 1)
        scraper.frequency = 10
        self.thread_runner_service.update_frecuency_thread(scraper)
        self.assertEqual(threads_running[0].frequency, scraper.frequency)
        self.thread_runner_service.stop_thread(scraper.currency)
        self.assertEqual(len(threads_running), 0)


    
    


