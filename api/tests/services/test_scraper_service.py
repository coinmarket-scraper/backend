from django.test import TestCase
from api.utils.custom_responses import (
    JsonResponse,
    JsonBadRequest,
    JsonCreatedResponse,
    JsonNotFoundResponse,
    JsonConflictResponse,
    HttpNoContentResponse
)
from api.services.scraper_service import ScraperService, Scraper

class ScraperServiceTest(TestCase):

    scraper_service = ScraperService(test_mode=True)

    @classmethod
    def setUpTestData(cls):
        Scraper.objects.create(currency='bitcoin', frequency=5)

    def setUp(self):
        self.valid_payload_creation = {
            'currency': 'etherum',
            'frequency': 5
        }
        self.duplicated_payload_creation = {
            'currency': 'bitcoin',
            'frequency': 5
        }
        self.valid_payload_patch = {
            'id': 1,
            'frequency': 5
        }
        self.not_found_payload_patch = {
            'id': 125,
            'frequency': 5
        }
        self.valid_payload_delete = {
            'id': 1
        }
        self.not_found_payload_delete = {
            'id': 125
        }
        self.invalid_payload = {
            'currensscy': 'etherum',
            'frequencsy': 5
        }
    
    def test_get_scrapers(self):
        response = self.scraper_service.get_scrapers()
        self.assertTrue(isinstance(response, JsonResponse))

    def test_create_scraper(self):
        response = self.scraper_service.create_scraper(self.valid_payload_creation)
        self.assertTrue(isinstance(response, JsonCreatedResponse))
    
    def test_duplicated_create_scraper(self):
        response = self.scraper_service.create_scraper(self.duplicated_payload_creation)
        self.assertTrue(isinstance(response, JsonConflictResponse))
    
    def test_invalid_create_scraper(self):
        response = self.scraper_service.create_scraper(self.invalid_payload)
        self.assertTrue(isinstance(response, JsonBadRequest))
    
    def test_delete_scraper(self):
        response = self.scraper_service.delete_scraper(self.valid_payload_delete)
        self.assertTrue(isinstance(response, HttpNoContentResponse))

    def test_not_found_delete_scraper(self):
        response = self.scraper_service.delete_scraper(self.not_found_payload_delete)
        self.assertTrue(isinstance(response, JsonNotFoundResponse))
    
    def test_invalid_delete_scraper(self):
        response = self.scraper_service.delete_scraper(self.invalid_payload)
        self.assertTrue(isinstance(response, JsonBadRequest))
    
    def test_patch_scraper(self):
        response = self.scraper_service.patch_scraper(self.valid_payload_patch)
        self.assertTrue(isinstance(response, JsonResponse))
    
    def test_not_found_patch_scraper(self):
        response = self.scraper_service.patch_scraper(self.not_found_payload_patch)
        self.assertTrue(isinstance(response, JsonNotFoundResponse))
    
    def test_invalid_patch_scraper(self):
        response = self.scraper_service.delete_scraper(self.invalid_payload)
        self.assertTrue(isinstance(response, JsonBadRequest))

    
    


