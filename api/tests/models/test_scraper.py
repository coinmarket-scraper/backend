from django.test import TestCase
from api.models import Scraper

class ScraperModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Scraper.objects.create(currency='bitcoin', frequency=5)

    def test_currency_max_length(self):
        scraper = Scraper.objects.get(id=1)
        max_length = scraper._meta.get_field('currency').max_length
        self.assertEqual(max_length, 15)
    
    def test_currency_unique(self):
        scraper = Scraper.objects.get(id=1)
        unique = scraper._meta.get_field('currency').unique
        self.assertEqual(unique, True)

    def test_value_null(self):
        scraper = Scraper.objects.get(id=1)
        null = scraper._meta.get_field('value').null
        self.assertEqual(null, True)

    def test_created_at_auto_now_add(self):
        scraper = Scraper.objects.get(id=1)
        created_at = scraper._meta.get_field('created_at').auto_now_add
        self.assertEqual(created_at, True)
    
    def test_skip_value_updated_at(self):
        scraper = Scraper.objects.get(id=1)
        value_updated_at_old = scraper.value_updated_at
        scraper.frequency = 4
        scraper.save(skip_value_updated_at=True)
        self.assertEqual(scraper.value_updated_at, value_updated_at_old)

    def test_serialize(self):
        scraper = Scraper.objects.get(id=1)
        serialize = {
            'id': scraper.pk,
            'frequency': scraper.frequency,
            'currency': scraper.currency,
            'value': scraper.value,
            'value_updated_at': scraper.value_updated_at,
            'created_at': scraper.created_at
        }
        self.assertEqual(scraper.serialize(), serialize)