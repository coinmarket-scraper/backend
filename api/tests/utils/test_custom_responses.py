from django.test import TestCase
from api.utils.custom_responses import (
    JsonBadRequest,
    JsonCreatedResponse,
    JsonNotFoundResponse,
    JsonConflictResponse,
    HttpNoContentResponse
)
from http import HTTPStatus

class ScraperViewTest(TestCase):

    def test_json_response(self):
        instance = JsonBadRequest({})
        self.assertEqual(instance.status_code, HTTPStatus.BAD_REQUEST)
    
    def test_json_created_response(self):
        instance = JsonCreatedResponse({})
        self.assertEqual(instance.status_code, HTTPStatus.CREATED)
    
    def test_json_not_found_response(self):
        instance = JsonNotFoundResponse({})
        self.assertEqual(instance.status_code, HTTPStatus.NOT_FOUND)
    
    def test_json_conflict_response(self):
        instance = JsonConflictResponse({})
        self.assertEqual(instance.status_code, HTTPStatus.CONFLICT)
    
    def test_http_no_content_response(self):
        instance = HttpNoContentResponse({})
        self.assertEqual(instance.status_code, HTTPStatus.NO_CONTENT)