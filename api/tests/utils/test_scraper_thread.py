from django.test import TestCase
from api.utils.scraper_thread import ScraperThread, Scraper

class ScraperViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Scraper.objects.create(currency='etherum', frequency=5)

    def test_run_thread(self):
        scraper = Scraper.objects.get(currency='etherum')
        scraper_thread = ScraperThread(scraper)
        scraper_thread.start()
        self.assertEqual(scraper_thread.is_alive(), True)
        scraper_thread.live = False
        scraper_thread.join()
        self.assertEqual(scraper_thread.is_alive(), False)
        