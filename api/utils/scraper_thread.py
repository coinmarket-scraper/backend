from api.models import Scraper
from api.scrapers.currency_scraper import CurrencyScraper
import threading
from time import sleep

class ScraperThread(threading.Thread):
    
    live = True
    __max_attempts = 5

    def __init__(self, scraper: Scraper):
        threading.Thread.__init__(self, name=scraper.currency) 
        self.frequency = scraper.frequency
        self.currency = scraper.currency
        self.name = scraper.currency        
    
    def run(self):
        currency_scraper = CurrencyScraper(self.currency)
        while self.live:
            try:
                thread = threading.Thread(target=currency_scraper.run, name='scraper {}'.format(self.name))
                thread.start()
                sleep(self.frequency)
                thread.join()
            except BaseException:
                self.__max_attempts += -1
                if not self.__max_attempts:
                    self.live = False

    