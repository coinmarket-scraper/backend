from django.http import JsonResponse, HttpResponse
from http import HTTPStatus

class JsonBadRequest(JsonResponse):
    status_code = HTTPStatus.BAD_REQUEST

class JsonCreatedResponse(JsonResponse):
    status_code = HTTPStatus.CREATED

class JsonNotFoundResponse(JsonResponse):
    status_code = HTTPStatus.NOT_FOUND

class JsonConflictResponse(JsonResponse):
    status_code = HTTPStatus.CONFLICT

class HttpNoContentResponse(HttpResponse):
    status_code = HTTPStatus.NO_CONTENT    