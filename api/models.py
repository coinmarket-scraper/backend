from django.db import models
from django.utils import timezone


class Scraper(models.Model):
    currency = models.CharField(max_length=15, unique=True)
    frequency = models.IntegerField()
    value = models.FloatField(null=True)
    value_updated_at = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not kwargs.pop('skip_value_updated_at', False):
            self.value_updated_at = timezone.now()
        super(Scraper, self).save(*args, **kwargs)
    
    def serialize(self) -> dict:
        """
        return a dict with the atributes of the model.
        """
        return {
            'id': self.pk,
            'frequency': self.frequency,
            'currency': self.currency,
            'value': self.value,
            'value_updated_at': self.value_updated_at,
            'created_at': self.created_at
        }