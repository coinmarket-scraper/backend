from django.views.generic import View
from api.services.scraper_service import ScraperService
import json

class ScraperAPI(View):
    scraper_service = ScraperService()

    def get(self, request, *args, **kwargs):
        return self.scraper_service.get_scrapers()

    def post(self, request, *args, **kwargs):
        json_body = json.loads(request.body)
        return self.scraper_service.create_scraper(json_body)

    def put(self, request, *args, **kwargs):
        json_body = json.loads(request.body)
        return self.scraper_service.patch_scraper(json_body)

    def delete(self, request, *args, **kwargs):
        json_body = json.loads(request.body)
        return self.scraper_service.delete_scraper(json_body)
