import threading
from api.models import Scraper
from django.conf import settings
from api.scrapers.currency_scraper import CurrencyScraper
from api.utils.scraper_thread import ScraperThread
from django.db.utils import OperationalError
from typing import List
import logging

ThreadList = List[ScraperThread]
threads_running: ThreadList = []

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )

class ThreadRunnerService():

    MAX_THREADS = settings.MAX_THREADS

    def __init__(self, test_mode: bool = False):
        self.__test_mode = test_mode

    def __get_scrapers(self):
        return Scraper.objects.all()
    
    def __check_if_is_valid_creation(self, currency) -> bool:
        """
        check if is a valid creation for a new thread.
        return True if the name of currency not exists in the current threads running.
        """
        for thread in threads_running:
            if thread.is_alive() and thread.name == currency:
                return False
        return True if len(threads_running) < self.MAX_THREADS else False

    def __get_index(self, currency) -> int:
        index = -1
        for thread in threads_running:
            if thread.name == currency:
                return threads_running.index(thread)
        return index
    
    def run_inital_scrapers(self):
        try:
            scrapers = self.__get_scrapers()
            for scraper in scrapers:
                if self.__check_if_is_valid_creation(scraper.currency):
                    scraper_runner_service = ScraperThread(scraper)
                    scraper_runner_service.start()
                    threads_running.append(scraper_runner_service)
        except OperationalError:
            pass
    
    def run_new_currency_thread(self, scraper: Scraper):
        if not self.__check_if_is_valid_creation(scraper.currency):
            return None
        logging.debug('starting thread {}'.format(scraper.currency))
        scraper_runner_service = ScraperThread(scraper)
        scraper_runner_service.start()
        threads_running.append(scraper_runner_service)
        logging.debug(threads_running)
        
    def update_frecuency_thread(self, scraper: Scraper) -> None:
        index = self.__get_index(scraper.currency)
        if index != -1:
            logging.debug('updating thread {}'.format(scraper.currency))
            threads_running[index].frequency = scraper.frequency
        return None
    
    def stop_thread(self, currency) -> None:
        index = self.__get_index(currency)
        if index != -1:
            logging.debug('stoping thread {}'.format(currency))
            threads_running[index].live = False
            if self.__test_mode:
                threads_running[index].join()
            threads_running.pop(index)   
        return None
        
            

        

