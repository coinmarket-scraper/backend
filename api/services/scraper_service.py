from api.models import Scraper
from django.db import IntegrityError
from api.utils.custom_responses import (
    JsonResponse,
    JsonBadRequest,
    JsonCreatedResponse,
    JsonNotFoundResponse,
    JsonConflictResponse,
    HttpNoContentResponse
)
from api.services.thread_runner_service import ThreadRunnerService
import operator
from django.core import serializers

class ScraperService():

    def __init__(self, test_mode: bool = False):
        self.__test_mode = test_mode

    PATCH_REQUEST = 'PATCH'
    POST_REQUEST = 'POST'
    DELETE_REQUEST = 'DELETE'
    __thread_runner_service = ThreadRunnerService()

    def __has_unnecessary_fields(self, payload: dict, necessary_fields: list) -> [dict, bool]:
        """
            this method return [dict, bool]:
            dict with the unnecessary fields if is a bad request, empty if not.
            bool with true in case of a invalid payload, false if not.
        """
        response_dict = {}
        for key in payload:
            if key not in necessary_fields:
                response_dict[key] = '{} is unnecessary in the payload, delete it.'.format(key)
        return response_dict, bool(response_dict)

    def __validate_payload(self, payload: dict, type_request: str) -> [dict, bool]:
        """
            this method return [dict, bool]:
            dict with the missing fields if is a bad request, empty if not.
            bool with true in case of a valid payload, false if not.
        """
        response_dict = {}
        if type_request == self.PATCH_REQUEST:
            necessary_fields = ['id', 'frequency']
            if not payload.get('id'):
                response_dict['id'] = 'id field is required.'
            if not payload.get('frequency'):
                response_dict['frequency'] = 'frequency field is required.'
            unnecesary_fields, _ = self.__has_unnecessary_fields(payload, necessary_fields)
            if _:
                response_dict.update(unnecesary_fields)
        elif type_request == self.POST_REQUEST:
            necessary_fields = ['frequency', 'currency']
            if not payload.get('frequency'):
                response_dict['frequency'] = 'frequency field is required.'
            if not payload.get('currency'):
                response_dict['currency'] = 'currency field is required.'
            unnecesary_fields, _ = self.__has_unnecessary_fields(payload, necessary_fields)
            if _:
                response_dict.update(unnecesary_fields)
        elif type_request == self.DELETE_REQUEST:
            necessary_fields = ['id']
            if not payload.get('id'):
                response_dict['id'] = 'id field is required.'
            unnecesary_fields, _ = self.__has_unnecessary_fields(payload, necessary_fields)
            if _:
                response_dict.update(unnecesary_fields)
        return response_dict, operator.not_(bool(response_dict))
    
    def get_scrapers(self) -> JsonResponse:
        scrapers = Scraper.objects.all()
        json_list = [scraper.serialize() for scraper in scrapers]
        return JsonResponse({'scrapers': json_list})

    def create_scraper(self, payload) -> JsonResponse:
        errors, _ = self.__validate_payload(payload, self.POST_REQUEST)
        if not _:
            return JsonBadRequest(errors)
        try:
            scraper = Scraper.objects.create(**payload)
            if not self.__test_mode:
                self.__thread_runner_service.run_new_currency_thread(scraper)
            return JsonCreatedResponse(scraper.serialize())
        except IntegrityError:
            return JsonConflictResponse({'detail' : 'Duplicated currency'})

    def patch_scraper(self, payload) -> JsonResponse:
        errors, _ = self.__validate_payload(payload, self.PATCH_REQUEST)
        if not _:
            return JsonBadRequest(errors)
        try:
            scraper = Scraper.objects.get(pk=payload['id'])
            scraper.frequency = payload.get('frequency', scraper.frequency)
            scraper.save(skip_value_updated_at=True)
            if not self.__test_mode:
                self.__thread_runner_service.update_frecuency_thread(scraper)
            return JsonResponse(scraper.serialize())
        except Scraper.DoesNotExist:
            return JsonNotFoundResponse({'detail' : 'Scraper not found'})
        except IntegrityError:
            return JsonConflictResponse({'detail' : 'Duplicated currency'})
    
    def delete_scraper(self, payload) -> JsonResponse:
        errors, _ = self.__validate_payload(payload, self.DELETE_REQUEST)
        if not _:
            return JsonBadRequest(errors)
        try:
            scraper = Scraper.objects.get(pk=payload['id'])
            if not self.__test_mode:
                self.__thread_runner_service.stop_thread(scraper.currency)
            scraper.delete()
            return HttpNoContentResponse()
        except Scraper.DoesNotExist:
            return JsonNotFoundResponse({'detail' : 'Scraper not found'})



        



