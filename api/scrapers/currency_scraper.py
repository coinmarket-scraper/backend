import requests
from django.conf import settings
from django.db.utils import OperationalError
from api.models import Scraper
import re

class CurrencyScraper():

    base_url = settings.URL_COINMARKETCAP

    def __init__(self, currency: str):
        self.currency = currency
        self.url = '{}/{}/'.format(self.base_url, currency)

    def __clean_number(self, number: str) -> str:
        clean_number = number.replace('$', '').replace(',', '')
        return clean_number

    def __get_price(self, response: requests.Response) -> float:
        content = str(response.content)
        price = re.findall(r'<span class="cmc-details-panel-price__price">(.*?)</span>|$', content)[0]
        clean_price = self.__clean_number(price) if price else "0"
        return float(clean_price)

    def __get_scraper_model(self) -> Scraper:
        try:
            return Scraper.objects.get(currency=self.currency)
        except Scraper.DoesNotExist:
            return None
        except OperationalError:
            return None

    def __save_price(self, price):
        scraper = self.__get_scraper_model()
        if scraper:
            scraper.value = price
            scraper.save()

    def __get_response(self):
        return requests.get(self.url)

    def run_test(self):
        response = self.__get_response()
        price = self.__get_price(response)
        return price

    def run(self):
        response = self.__get_response()
        price = self.__get_price(response)
        self.__save_price(price)
        




    