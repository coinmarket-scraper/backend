#!/bin/bash

# Making migrations
python3 ./manage.py makemigrations
# Running migrations
python3 ./manage.py migrate
# Running tests
python3 ./manage.py test --verbosity 2